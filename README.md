# java-persistence

Code examples demonstrating persistence in Java with JPA and JDBC:

- `BookDao` defines CRUD operations for entity `Book`
- `JpaBookDao` implements `BookDao` using the Java Persistence API
- `JdbcBookDao` implements `BookDao` using JDBC and SQL statements
- Includes support for Derby embedded DB and MySQL

