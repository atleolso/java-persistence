package edu.ntnu.idatt2001.jpa;

import edu.ntnu.idatt2001.Book;
import edu.ntnu.idatt2001.BookDao;
import edu.ntnu.idatt2001.DataAccessException;
import org.junit.jupiter.api.*;

import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityManagerFactory;
import jakarta.persistence.Persistence;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Integration tests for JpaBookDao.
 */
class JpaBookDaoTest {

    private static EntityManagerFactory emf;
    private static EntityManager entityManager;
    private static BookDao bookDAO;
    private static Book sampleBook;

    @BeforeAll
    static void initResources() {
        emf = Persistence.createEntityManagerFactory("pu-derby-test");
        entityManager = emf.createEntityManager();
        bookDAO = new JpaBookDao(entityManager);
        sampleBook = new Book("978-0142410370", "Mathilda", "Roald Dahl");
    }

    @AfterAll
    static void closeResources() {
        if (entityManager != null) {
            entityManager.close();
        }
        if (emf != null) {
            emf.close();
        }
    }

    @Test
    void createBook() {
        try {
            bookDAO.create(sampleBook);
            assertTrue(bookDAO.exists(sampleBook.getIsbn()));
        } catch (DataAccessException e) {
            fail();
        } finally {
            try {
                bookDAO.delete(sampleBook.getIsbn());
            } catch (DataAccessException e) {
                fail();
            }
        }
    }

    @Test
    void createBookWhenItAlreadyExists() {
        try {
            bookDAO.create(sampleBook);
            assertThrows(Exception.class, () -> bookDAO.create(sampleBook));
        } catch (DataAccessException e) {
            fail();
        } finally {
            try {
                bookDAO.delete(sampleBook.getIsbn());
            } catch (DataAccessException e) {
                fail();
            }
        }
    }

    @Test
    void getBook() {
        try {
            bookDAO.create(sampleBook);
            Book fetchedBook = bookDAO.get(sampleBook.getIsbn());
            assertEquals(sampleBook, fetchedBook);
        } catch (DataAccessException e) {
            fail();
        } finally {
            try {
                bookDAO.delete(sampleBook.getIsbn());
            } catch (Exception e) {
                fail();
            }
        }
    }

    @Test
    void getBookWhenItDoesNotExist() {
        assertThrows(DataAccessException.class, () -> bookDAO.get("000-0000000000"));
    }

    @Test
    void bookExists() {
        try {
            bookDAO.create(sampleBook);
            boolean exists = bookDAO.exists(sampleBook.getIsbn());
            assertTrue(exists);
        } catch (DataAccessException e) {
            fail();
        } finally {
            try {
                bookDAO.delete(sampleBook.getIsbn());
            } catch (DataAccessException e) {
                fail();
            }
        }
    }

    @Test
    void bookDoesNotExist() {
        try {
            boolean exists = bookDAO.exists(sampleBook.getIsbn());
            assertFalse(exists);
        } catch (DataAccessException e) {
            fail();
        }
    }

    @Test
    void deleteBook() {
        try {
            bookDAO.create(sampleBook);
            boolean isDeleted = bookDAO.delete(sampleBook.getIsbn());
            assertTrue(isDeleted);
        } catch (DataAccessException e) {
            fail();
        }
    }

    @Test
    void deleteBookWhenItDoesNotExist() {
        try {
            boolean isDeleted = bookDAO.delete(sampleBook.getIsbn());
            assertFalse(isDeleted);
        } catch (DataAccessException e) {
            fail();
        }
    }

    @Test
    void updateBook() {
        try {
            bookDAO.create(sampleBook);
            sampleBook.setTitle("Updated title");
            boolean isUpdated = bookDAO.update(sampleBook);
            assertTrue(isUpdated);
        } catch (DataAccessException e) {
            fail();
        } finally {
            try {
                bookDAO.delete(sampleBook.getIsbn());
            } catch (DataAccessException e) {
                fail();
            }
        }
    }

    @Test
    void updateBookWhenItDoesNotExist() {
        try {
            sampleBook.setTitle("Updated title");
            boolean isUpdated = bookDAO.update(sampleBook);
            assertFalse(isUpdated);
        } catch (DataAccessException e) {
            fail();
        }
    }

    @Test
    void getBookByAuthor() {
        try {
            bookDAO.create(sampleBook);
            List<Book> books = bookDAO.getByAuthor(sampleBook.getAuthor());
            assertEquals(1, books.size());
            assertTrue(books.contains(sampleBook));
        } catch (DataAccessException e) {
            fail();
        } finally {
            try {
                bookDAO.delete(sampleBook.getIsbn());
            } catch (DataAccessException e) {
                fail();
            }
        }
    }

    @Test
    void getBooksByAuthor() {
        String roaldDahl = "Roald Dahl";
        Book mathilda = new Book("978-0142410370", "Mathilda", roaldDahl);
        Book fantasticMrFox = new Book("978-0142410349", "Fantastic Mr Fox", roaldDahl);
        try {
            bookDAO.create(mathilda);
            bookDAO.create(fantasticMrFox);
            List<Book> books = bookDAO.getByAuthor(roaldDahl);
            assertEquals(2, books.size());
            assertTrue(books.contains(mathilda));
            assertTrue(books.contains(fantasticMrFox));
        } catch (DataAccessException e) {
            fail();
        } finally {
            try {
                bookDAO.delete(mathilda.getIsbn());
                bookDAO.delete(fantasticMrFox.getIsbn());
            } catch (DataAccessException e) {
                fail();
            }
        }
    }
}