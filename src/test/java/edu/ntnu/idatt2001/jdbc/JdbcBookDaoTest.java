package edu.ntnu.idatt2001.jdbc;

import edu.ntnu.idatt2001.Book;
import edu.ntnu.idatt2001.BookDao;
import edu.ntnu.idatt2001.DataAccessException;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.List;
import java.util.Properties;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Integration tests for JdbcBookDao.
 */
class JdbcBookDaoTest {

    private static Connection connection;
    private static BookDao bookDAO;
    private static Book sampleBook;

    @BeforeAll
    static void initResources() throws Exception {
        //connection config must be placed in a properties-file in test/resources
        Properties dbProps = PropertiesReader.read("src/test/resources/db.properties");
        String url = dbProps.getProperty("jdbc.url");
        String user = dbProps.getProperty("jdbc.user");
        String password = dbProps.getProperty("jdbc.password");

        connection = DriverManager.getConnection(url, user, password);
        bookDAO = new JdbcBookDao(connection);
        sampleBook = new Book("978-0142410370", "Mathilda", "Roald Dahl");
    }

    @AfterAll
    static void closeResources() throws SQLException {
        if (connection != null) {
            connection.close();
        }
    }

    @Test
    void createBook() {
        try {
            bookDAO.create(sampleBook);
            assertTrue(bookDAO.exists(sampleBook.getIsbn()));
        } catch (DataAccessException e) {
           fail();
        } finally {
            try {
                bookDAO.delete(sampleBook.getIsbn());
            } catch (DataAccessException e) {
                fail();
            }
        }
    }

    @Test
    void createBookWhenItAlreadyExists() {
        try {
            bookDAO.create(sampleBook);
            assertThrows(DataAccessException.class, () -> bookDAO.create(sampleBook));
        } catch (DataAccessException e) {
            fail();
        } finally {
            try {
                bookDAO.delete(sampleBook.getIsbn());
            } catch (DataAccessException e) {
                fail();
            }
        }
    }

    @Test
    void getBook() {
        try {
            bookDAO.create(sampleBook);
            Book fetchedBook = bookDAO.get(sampleBook.getIsbn());
            assertEquals(sampleBook, fetchedBook);
        } catch (DataAccessException e) {
            fail();
        } finally {
            try {
                bookDAO.delete(sampleBook.getIsbn());
            } catch (DataAccessException e) {
                fail();
            }
        }
    }

    @Test
    void getBookWhenItDoesNotExist() {
        assertThrows(DataAccessException.class, () -> bookDAO.get("000-0000000000"));
    }

    @Test
    void bookExists() {
        try {
            bookDAO.create(sampleBook);
            boolean exists = bookDAO.exists(sampleBook.getIsbn());
            assertTrue(exists);
        } catch (DataAccessException e) {
            fail();
        } finally {
            try {
                bookDAO.delete(sampleBook.getIsbn());
            } catch (DataAccessException e) {
                fail();
            }
        }
    }

    @Test
    void bookDoesNotExist() {
        try {
            boolean exists = bookDAO.exists(sampleBook.getIsbn());
            assertFalse(exists);
        } catch (DataAccessException e) {
            fail();
        }
    }

    @Test
    void deleteBook() {
        try {
            bookDAO.create(sampleBook);
            boolean isDeleted = bookDAO.delete(sampleBook.getIsbn());
            assertTrue(isDeleted);
        } catch (DataAccessException e) {
            fail();
        }
    }

    @Test
    void deleteBookWhenItDoesNotExist() {
        try {
            boolean isDeleted = bookDAO.delete(sampleBook.getIsbn());
            assertFalse(isDeleted);
        } catch (DataAccessException e) {
            fail();
        }
    }

    @Test
    void updateBook() {
        try {
            bookDAO.create(sampleBook);
            sampleBook.setTitle("Updated title");
            boolean isUpdated = bookDAO.update(sampleBook);
            assertTrue(isUpdated);
        } catch (DataAccessException e) {
           fail();
        } finally {
            try {
                bookDAO.delete(sampleBook.getIsbn());
            } catch (DataAccessException e) {
                fail();
            }
        }
    }

    @Test
    void updateBookWhenItDoesNotExist() {
        try {
            sampleBook.setTitle("Updated title");
            boolean isUpdated = bookDAO.update(sampleBook);
            assertFalse(isUpdated);
        } catch (DataAccessException e) {
            fail();
        }
    }

    @Test
    void getBookByAuthor() {
        try {
            bookDAO.create(sampleBook);
            List<Book> books = bookDAO.getByAuthor(sampleBook.getAuthor());
            assertEquals(1, books.size());
            assertTrue(books.contains(sampleBook));
        } catch (DataAccessException e) {
            fail();
        } finally {
            try {
                bookDAO.delete(sampleBook.getIsbn());
            } catch (DataAccessException e) {
                fail();
            }
        }
    }

    @Test
    void getBooksByAuthor() {
        String roaldDahl = "Roald Dahl";
        Book mathilda = new Book("978-0142410370", "Mathilda", roaldDahl);
        Book fantasticMrFox = new Book("978-0142410349", "Fantastic Mr Fox", roaldDahl);
        try {
            bookDAO.create(mathilda);
            bookDAO.create(fantasticMrFox);
            List<Book> books = bookDAO.getByAuthor(roaldDahl);
            assertEquals(2, books.size());
            assertTrue(books.contains(mathilda));
            assertTrue(books.contains(fantasticMrFox));
        } catch (DataAccessException e) {
            fail();
        } finally {
            try {
                bookDAO.delete(mathilda.getIsbn());
                bookDAO.delete(fantasticMrFox.getIsbn());
            } catch (DataAccessException e) {
                fail();
            }
        }
    }
}