package edu.ntnu.idatt2001.jpa;

import edu.ntnu.idatt2001.Book;
import edu.ntnu.idatt2001.BookDao;

import jakarta.persistence.*;
import java.util.List;

public class JpaApp {

    public static void main(String[] args) {
        EntityManagerFactory emf = null;
        EntityManager entityManager = null;

        try {
            emf = Persistence.createEntityManagerFactory("pu-mysql-dev");
            entityManager = emf.createEntityManager();
            BookDao bookDao = new JpaBookDao(entityManager);

            bookDao.create(new Book("978-0142410370", "Mathilda", "Roald Dahl"));
            bookDao.create(new Book("978-0142410349", "Fantastic Mr Fox", "Roald Dahl"));

            List<Book> booksByRoaldDahl = bookDao.getByAuthor("Roald Dahl");

            Book mathilda = bookDao.get("978-0142410370");
            mathilda.setTitle("Mathilda (Paperback edition)");
            bookDao.update(mathilda);

            for (Book book : booksByRoaldDahl) {
                bookDao.delete(book.getIsbn());
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (entityManager != null) {
                entityManager.close();
            }
            if (emf != null) {
                emf.close();
            }
        }
    }
}
