package edu.ntnu.idatt2001.jpa;

import edu.ntnu.idatt2001.Book;
import edu.ntnu.idatt2001.BookDao;
import edu.ntnu.idatt2001.DataAccessException;
import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityTransaction;
import jakarta.persistence.TypedQuery;

import java.util.List;

/**
 * DAO for Books using JPA.
 */
public class JpaBookDao implements BookDao {

    private final EntityManager entityManager;

    public JpaBookDao(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    public Book get(String isbn) throws DataAccessException {
        try {
            Book book = entityManager.find(Book.class, isbn);
            if (book == null) {
                throw new DataAccessException("Book with ISBN '" + isbn + "' does not exist.");
            } else {
                return book;
            }
        } catch (Exception e) {
            throw new DataAccessException("Unable to get book with ISBN '" + isbn + "'. " + e.getMessage(), e);
        }
    }


    @Override
    public boolean exists(String isbn) throws DataAccessException {
        try {
            return (entityManager.find(Book.class, isbn) != null);
        } catch (Exception e) {
            throw new DataAccessException("Unable to check if book with ISBN '" + isbn + "' exists. " + e.getMessage(), e);
        }
    }

    public void create(Book book) throws DataAccessException {
        EntityTransaction transaction = null;

        try {
            if (!exists(book.getIsbn())) {
                transaction = entityManager.getTransaction();
                transaction.begin();
                entityManager.persist(book);
                transaction.commit();
            } else {
                throw new DataAccessException("Book with ISBN '" + book.getIsbn() + "' already exists");
            }
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            throw new DataAccessException("Unable to create book " + book + ". " + e.getMessage(), e);
        }
    }

    public boolean update(Book book) throws DataAccessException {
        EntityTransaction transaction = null;

        try {
            if (exists(book.getIsbn())) {
                transaction = entityManager.getTransaction();
                transaction.begin();
                entityManager.merge(book);
                transaction.commit();
                return true;
            } else {
                return false;
            }
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            throw new DataAccessException("Unable to update book with ISBN '" + book.getIsbn() + "'. " + e.getMessage(), e);
        }
    }

    public boolean delete(String isbn) throws DataAccessException {
        EntityTransaction transaction = null;

        try {
            if (exists(isbn)) {
                Book book = get(isbn);
                transaction = entityManager.getTransaction();
                transaction.begin();
                entityManager.remove(book);
                transaction.commit();
                return true;
            } else {
                return false;
            }
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            throw new DataAccessException("Unable to delete book with ISBN '" + isbn + "'. " + e.getMessage(), e);
        }
    }

    public List<Book> getByAuthor(String author) throws DataAccessException {
        try {
            TypedQuery<Book> query = entityManager.createQuery("SELECT b FROM Book b WHERE " +
                    "b.author = :author", Book.class);
            query.setParameter("author", author);
            return query.getResultList();
        } catch (Exception e) {
            throw new DataAccessException("Unable to get books for author '" + author + "'. " + e.getMessage(), e);
        }
    }
}
