package edu.ntnu.idatt2001;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import java.util.Objects;

@Entity
// @Table(name="TABLE_NAME") if table name ≠ class name
// See https://docs.oracle.com/javaee/7/api/javax/persistence/Table.html
public class Book {

    @Id
    private String isbn;

    // @Column(name="COLUMN_NAME") if column name ≠ attribute name
    // See https://docs.oracle.com/javaee/7/api/javax/persistence/Column.html
    private String title;
    private String author;


    public Book(String isbn, String title, String author) {
        this.isbn = isbn;
        this.title = title;
        this.author = author;
    }

    //JPA requires no-args constructor
    //See https://docs.oracle.com/javaee/6/tutorial/doc/bnbqa.html#bnbqb
    public Book() {
    }

    public String getIsbn() {
        return isbn;
    }

    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    @Override
    public String toString() {
        return "Book{" +
                "isbn='" + isbn + '\'' +
                ", title='" + title + '\'' +
                ", author='" + author + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Book book = (Book) o;
        return Objects.equals(isbn, book.isbn) && Objects.equals(title, book.title) && Objects.equals(author, book.author);
    }

    @Override
    public int hashCode() {
        return Objects.hash(isbn, title, author);
    }
}
