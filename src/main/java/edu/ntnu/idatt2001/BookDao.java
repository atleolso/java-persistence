package edu.ntnu.idatt2001;

import java.util.List;

/**
 * The {@code BookDao} interface facilitates access to a repository of books.
 */
public interface BookDao {

    /**
     * Gets a book for the specified ISBN if it exists in the repository.
     * @param isbn a unique ID for a book.
     * @return a book for the given ISBN if it exists in the repository.
     * @throws Exception if no book exists for the given ISBN or if a data access error occurs.
     */
    Book get(String isbn) throws DataAccessException; // TODO: better to use Optional than Exception if book is not found

    /**
     * Gets all books in the repository by a given author.
     * @param author the full name of the author.
     * @return a list of books in the repository by the given author.
     * @throws Exception if a data access error occurs.
     */
    List<Book> getByAuthor(String author) throws DataAccessException;

    /**
     * Checks if a book exists in the repository.
     * @param isbn a unique ID for a book.
     * @return true if the book exists in the repository, false otherwise.
     * @throws Exception if a data access error occurs.
     */
    boolean exists(String isbn) throws DataAccessException;

    /**
     * Persists a book that does not already exist in the repository.
     * @param book a new book that does not exist in the repository.
     * @throws Exception if book already exists in the repository or if a data access error occurs.
     */
    void create(Book book) throws DataAccessException;

    /**
     * Updates a book in the repository. The book will be overwritten with data
     * from parameter {@code book}.
     * @param book the book in the repository will be overwritten with data from this instance.
     * @return true if the book was updated, false otherwise.
     * @throws Exception if a data access error occurs.
     */
    boolean update(Book book) throws DataAccessException;

    /**
     * Deletes a book from the repository.
     * @param isbn unique ID of the book to be deleted.
     * @return true if the book was deleted, false otherwise.
     * @throws Exception if a data access error occurs.
     */
    boolean delete(String isbn) throws DataAccessException;

}
