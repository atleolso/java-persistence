package edu.ntnu.idatt2001.jdbc;

import edu.ntnu.idatt2001.Book;
import edu.ntnu.idatt2001.BookDao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.List;
import java.util.Properties;

public class JdbcApp {

    public static void main(String... args) {
        Connection connection = null;

        try {
            Properties dbProps = PropertiesReader.read("src/main/resources/db.properties");
            String url = dbProps.getProperty("jdbc.url");
            String user = dbProps.getProperty("jdbc.user");
            String password = dbProps.getProperty("jdbc.password");

            connection = DriverManager.getConnection(url, user, password);
            BookDao bookDao = new JdbcBookDao(connection);

            bookDao.create(new Book("978-0142410370", "Mathilda", "Roald Dahl"));
            bookDao.create(new Book("978-0142410349", "Fantastic Mr Fox", "Roald Dahl"));

            List<Book> booksByRoaldDahl = bookDao.getByAuthor("Roald Dahl");

            Book mathilda = bookDao.get("978-0142410370");
            mathilda.setTitle("Mathilda (Paperback edition)");
            bookDao.update(mathilda);

            for (Book book : booksByRoaldDahl) {
                bookDao.delete(book.getIsbn());
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
