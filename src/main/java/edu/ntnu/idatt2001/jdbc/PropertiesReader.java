package edu.ntnu.idatt2001.jdbc;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class PropertiesReader {

    public static Properties read(String path) throws IOException {
        try (InputStream is = new FileInputStream(path)) {
            Properties properties = new Properties();
            properties.load(is);
            return properties;
        } catch (IOException e) {
            throw new IOException("Unable to load properties file " + path + ": " + e.getMessage());
        }
    }
}
