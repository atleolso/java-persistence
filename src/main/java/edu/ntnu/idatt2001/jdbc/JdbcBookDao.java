package edu.ntnu.idatt2001.jdbc;

import edu.ntnu.idatt2001.Book;
import edu.ntnu.idatt2001.BookDao;
import edu.ntnu.idatt2001.DataAccessException;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * DAO for books using JDBC.
 */
public class JdbcBookDao implements BookDao {

    private final Connection connection;

    public JdbcBookDao(Connection connection) {
        this.connection = connection;
    }

    public Book get(String isbn) throws DataAccessException {
        try (PreparedStatement ps = connection.prepareStatement("SELECT * from BOOK WHERE isbn=?")) {
            ps.setString(1, isbn);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                return new Book(rs.getString("isbn"), rs.getString("title"), rs.getString("author"));
            } else {
                throw new DataAccessException("Book with ISBN '" + isbn + "' does not exist.");
            }
        } catch (SQLException e) {
            throw new DataAccessException("Unable to get book with ISBN '" + isbn + "'. " + e.getMessage(), e);
        }
    }

    public boolean exists(String isbn) throws DataAccessException {
        try (PreparedStatement ps = connection.prepareStatement("SELECT 1 from BOOK WHERE isbn=?")) {
            ps.setString(1, isbn);
            return ps.executeQuery().next();
        } catch (SQLException e) {
            throw new DataAccessException("Unable to check if book with ISBN '" + isbn + "' exists. " + e.getMessage(), e);
        }
    }

    public void create(Book book) throws DataAccessException {
        try (PreparedStatement ps = connection.prepareStatement("INSERT INTO BOOK VALUES (?, ?, ?)")) {
            if (!exists(book.getIsbn())) {
                ps.setString(1, book.getIsbn());
                ps.setString(2, book.getTitle());
                ps.setString(3, book.getAuthor());
                ps.execute();
            } else {
                throw new DataAccessException("Book with ISBN '" + book.getIsbn() + "' already exists");
            }
        } catch (SQLException e) {
            throw new DataAccessException("Unable to create book " + book + ". " + e.getMessage(), e);
        }
    }

    public boolean update(Book book) throws DataAccessException {
        try (PreparedStatement ps = connection.prepareStatement("UPDATE BOOK SET title=?, author=? WHERE isbn=?")) {
            ps.setString(1, book.getTitle());
            ps.setString(2, book.getAuthor());
            ps.setString(3, book.getIsbn());
            return (ps.executeUpdate() == 1);

        } catch (SQLException e) {
            throw new DataAccessException("Unable to update book with ISBN '" + book.getIsbn() + "'. " + e.getMessage(), e);
        }
    }

    public boolean delete(String isbn) throws DataAccessException {
        try (PreparedStatement ps = connection.prepareStatement("DELETE FROM BOOK WHERE isbn=?")) {
            ps.setString(1, isbn);
            return (ps.executeUpdate() == 1);
        } catch (SQLException e) {
            throw new DataAccessException("Unable to delete book with ISBN '" + isbn + "'. " + e.getMessage(), e);
        }
    }

    public List<Book> getByAuthor(String author) throws DataAccessException {
        List<Book> booksByAuthor = new ArrayList<>();
        try (PreparedStatement ps = connection.prepareStatement("SELECT * FROM BOOK WHERE author = ?")) {
            ps.setString(1, author);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Book book = new Book(rs.getString("isbn"), rs.getString("title"), rs.getString("author"));
                booksByAuthor.add(book);
            }
            return booksByAuthor;
        } catch (SQLException e) {
            throw new DataAccessException("Unable to get books for author '" + author + "'. " + e.getMessage(), e);
        }
    }
}
